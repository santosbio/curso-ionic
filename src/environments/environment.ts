// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAtcsU9j2HOT67S7SEW2L5zYYYcD3FM7Zo',
    authDomain: 'ionic-firestore-tasks-57713.firebaseapp.com',
    databaseURL: 'https://ionic-firestore-tasks-57713.firebaseio.com',
    projectId: 'ionic-firestore-tasks-57713',
    storageBucket: '',
    messagingSenderId: '903927964077',
    appId: '1:903927964077:web:96220ef3e559b02c'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
